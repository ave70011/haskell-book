module Char where

import Data.Char

filterUpper :: String -> String
filterUpper = filter isUpper

firstUpper :: String -> String
firstUpper (x:xs) = toUpper x : xs

wordUpper :: String -> String
wordUpper = go [] where
  go acc [] = reverse acc
  go acc (x:xs) = go (toUpper x:acc) xs

main :: IO()
main = undefined
