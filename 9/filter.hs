module Filter where

filterMult :: [Integer] -> Integer -> [Integer]
filterMult l n = filter (\x -> x `mod` n == 0) l

articleFilter :: [String] -> [String]
articleFilter = filter (\x -> x /= "a" && x /= "an" && x /= "the")

main = length . filterMult [1..30] $ 3
