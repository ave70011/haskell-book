module PoemLines where

breakOn :: String -> Char -> [String]
breakOn s c = go s [] where
  go [] acc = reverse acc
  go s' acc = go (drop 1 . dropWhile (/= c) $ s') (takeWhile (/= c) s' : acc)

myWords :: String -> [String]
myWords x = breakOn x ' '

firstSen :: String
firstSen = "Tyger Tyger, burning bright\n"

secondSen :: String
secondSen = "In the forests of the night\n"

thirdSen :: String
thirdSen = "What immortal hand or eye\n"

fourthSen :: String
fourthSen = "Could frame thy fearful\
\ symmetry?"

sentences :: String
sentences = firstSen ++ secondSen ++ thirdSen ++ fourthSen

myLines :: String -> [String]
myLines x = breakOn x '\n'

shouldEqual = [
  "Tyger Tyger, burning bright"
  , "In the forests of the night"
  , "What immortal hand or eye"
  , "Could frame thy fearful symmetry?"
              ]

main :: IO()
main = print $ "Are they equal? " ++ show (myLines sentences == shouldEqual)
