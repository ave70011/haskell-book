module Cipher where

import Data.Char

a :: Int
a = ord 'a'
z :: Int
z = ord 'z'
capA :: Int
capA = ord 'A'
capZ :: Int
capZ = ord 'Z'

shiftUpper :: Char -> Int -> Char
shiftUpper char shift
  | (ord char + shift) > capZ = chr (ord char + shift - 26)
  | otherwise = chr (ord char + shift)

shiftLower :: Char -> Int -> Char
shiftLower char shift
  | (ord char + shift) > z = chr (ord char + shift - 26)
  | otherwise = chr (ord char + shift)

caesar :: String -> Int -> String
caesar s i = go s i [] where
  go [] _ acc = reverse acc
  go (x:xs) i' acc = go xs i' (x':acc) where
    x' = if isUpper x then
      shiftUpper x i else
      shiftLower x i

uncaesar :: String -> Int -> String
uncaesar s i = caesar s (-i)

main :: IO()
main = undefined
