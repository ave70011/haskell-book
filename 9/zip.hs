module Zip where

zip' :: [a] -> [b] -> [(a,b)]
zip' l1 l2 = zipWith' (,) l1 l2 

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f l1 l2 = go f l1 l2 [] where
  go _ [] _ acc = reverse acc
  go _ _ [] acc = reverse acc
  go f' (x:xs) (y:ys) acc = go f' xs ys (f' x y : acc)

main :: IO()
main = undefined
