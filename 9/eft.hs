module Eft where

eftBool :: Bool -> Bool -> [Bool]
eftBool x y
  | x == y = [x]
eftBool True _ = []
eftBool False True = [False, True]

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd x y 
  | x == y = [x]
eftOrd x GT = case x of
  EQ -> [EQ,GT]
  LT -> [LT,EQ,GT]
eftOrd LT EQ = [LT,EQ]
eftOrd GT _ = []

eftInt :: Int -> Int -> [Int]
eftInt x y = if x > y then
  [] else
  go x y [] where
  go x' y' acc
    | x' > y' = acc
    | otherwise = go x' (y'-1) (y':acc)

eftChar :: Char -> Char -> [Char]
eftChar x y = if x > y then
  [] else
    go x y [] where
    go x' y' acc
      | x' > y' = acc
      | otherwise = go x' (pred y') (y':acc)

main :: IO()
main = undefined
