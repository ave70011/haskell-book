i :: a -> a
i x = x

c :: a -> b -> a
c a b = a

c'' :: b -> a -> b
c'' b a = b

c' :: a -> b -> b
c' a b = b

r :: [a] -> [a]
r (x:xs) = xs

co :: (b -> c) -> (a -> b) -> a -> c
co bToC aToB a = bToC . aToB $ a

a :: (a->c) -> a -> a
a f x = x

a' :: (a -> b) -> a -> b
a' aToB a = aToB a
