module DividedBy where

data DividedResult a = Result (a, a) | DividedByZero deriving Show

dividedBy :: Integral a => a -> a -> DividedResult a
dividedBy num denom = go num denom 0
  where
    go _ 0 _ = DividedByZero
    go n d count | n < 0 && d < 0 = go (-n) (-d) count
    go n d count | n < 0 || d < 0 = go (-n) d count
    go n d count
          | n < d = Result (count, n)
          | otherwise = go (n - d) d (count + 1)

main :: IO()
main = undefined
