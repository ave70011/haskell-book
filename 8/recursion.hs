module Recursion where

sum' :: (Eq a, Num a) => a -> a
sum' n = go n 0 where
  go 0 s = s
  go n' s = go (n'-1) (s+ n')

multiply' :: (Integral a) => a -> a -> a
multiply' x y = go x y 0 where
  go _ 0 acc = acc
  go x' y' acc = go x' (y' - 1) (acc + x')

main :: IO()
main = undefined
