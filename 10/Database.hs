import Data.Time
import Data.Maybe

data DatabaseItem = DbString String
                  | DbNumber Integer
                  | DbDate UTCTime
                  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase = 
  [DbDate (UTCTime
          (fromGregorian 1911 5 1)
          (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbDate (UTCTime
           (fromGregorian 1921 5 1)
           (secondsToDiffTime 34123))
  ]

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate database = 
  let reducer :: DatabaseItem -> [UTCTime] -> [UTCTime]
      reducer (DbDate x) xs = x:xs
      reducer _ xs = xs in 
      foldr reducer [] database

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber database = 
  let reducer :: DatabaseItem -> [Integer] -> [Integer]
      reducer (DbNumber x) xs = x:xs
      reducer _ xs = xs in
      foldr reducer [] database

mostRecent :: [DatabaseItem] -> UTCTime
mostRecent database = 
  let reducer :: DatabaseItem -> Maybe UTCTime -> Maybe UTCTime
      reducer (DbDate x) Nothing = Just x
      reducer (DbDate x) (Just y) = if x > y then Just x else Just y
      reducer _ acc = acc in 
      fromJust . foldr reducer Nothing $ database

sumDb :: [DatabaseItem] -> Integer
sumDb database = 
  let reducer :: DatabaseItem -> Integer -> Integer
      reducer (DbNumber x) acc = x + acc
      reducer _ acc = acc in
      foldr reducer 0 database

avgDb :: [DatabaseItem] -> Double
avgDb database =
  let sum = fromIntegral . sumDb $ database
      filterFunction (DbNumber x) = True
      filterFunction _ = False
      count = fromIntegral . length . filter filterFunction $ database
   in sum/count

main :: IO ()
main = undefined
