thirdLetter :: String -> Char
thirdLetter = head . drop 2

letterIndex :: Int -> Char
letterIndex = (!!) "Curry is awesome!"

rvrs :: String -> String
rvrs s = (take 7 . drop 9 $ s)  ++ (take 4 . drop 5 $ s) ++ take 5 s
