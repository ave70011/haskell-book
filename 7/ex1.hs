module Ex1 where


tensDigit :: Integral a => a -> a
tensDigit x = d
  where
    xLast = x `div` 10
    d = xLast `mod` 10

tensDigit' :: Integral a => a -> a
tensDigit' x = d
  where
    xLast = fst $ x `divMod` 10
    d = snd $ xLast `divMod` 10

hunsDigit :: Integral a => a -> a
hunsDigit x = d2
  where
    xHund = x `div` 100
    d2 = xHund `mod` 10

foldBool :: a -> a -> Bool -> a
foldBool x y bool =
  case bool of
    True -> x
    False -> y

foldBool' :: a -> a -> Bool -> a
foldBool' x y bool
  | bool = x
  | otherwise = y

g :: (a -> b) -> (a,c) -> (b,c)
g aToB (a,c) = (aToB a, c)

